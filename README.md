# Jupyter in System Theory II

## Introduction

This repository serves as Sys-II Jupyter profile which works with the RWTH Jupyter server. To be more specific, it includes the following files
* `Dockerfile` which defines the linux environment. In the end, the packages of an `environment.yml` or here the `requirements.txt` are installed.

[Here is a quick start guide of the example profile on which this course builds upon](https://git.rwth-aachen.de/jupyter/example-profile/-/blob/master/Quickstart.ipynb).

## Installation

### Local Installation

To run the notebooks on your local machine, you may use [Anaconda](https://www.anaconda.com/) (using `pip` is also possible for experienced users. You have to install all the requirements listed in `environment.yml` and install the commands listed in `postBuild.sh`).

* Install [Anaconda](https://www.anaconda.com/).
* Download this repository to your local disk. You can download it as a zip-File or use `git`:  

  ```bash
  git clone git@git.rwth-aachen.de:acs/public/teaching/systemtheorie2/lecture-tutorials.git
  ```

* It is highly recommended to run the notebooks in an isolated Anaconda environment. You can create a new environment called `jupyter-sys2-profile` from the provided `environment.yml` by running the following command in the Anaconda prompt

  ```bash
  conda env create -f environment.yml
  ```
  
  This makes sure that all required packages are installed amd don't interfere with the packages in your base environment.
* Activate this environment with

  ```bash
  conda activate jupyter-sys2-profile
  ```

### Local Run

* Activate the environment  with `conda activate jupyter-sys2-profile`.
* Run JupyterLab  `jupyter lab`. In your browser, JupyterLab should start. You can then open `index.ipynb` for an overview over all notebooks.
* You can deactivate the environment with `conda deactivate`.

## Contact

* If you found a bug, please use the [issue tracker](https://git.rwth-aachen.de/acs/public/teaching/systemtheorie2/lecture-tutorials/-/issues).
* In all other cases, please contact [your system theory team](https://www.acs.eonerc.rwth-aachen.de/cms/E-ON-ERC-ACS/Studium/Lehrveranstaltungen/~bycsp/Systemtheorie/).

The code is licensed under the [MIT license](https://opensource.org/licenses/MIT).
