%Consider the state-space representation
A = [-1   0;
    1   -2]
B = [1; 0] 
C = [0 1]
D = 0
%Evaluate the solution from the exercise
%Tranformation matrix
V = [1 0;
    1  1]
%Inverse transformations matrix (could also be replaced with a command)
V1= [1 0;
    -1 1]
%Diagonal canonical form of A from the result of the exercise
Ad = [-1 0;
    0 -2]
%New state-space representation
Adc = V1*A*V
Bd = V1*B
Cd = C*V
%Verify whether the transformed system has diagonal canonical form?
if Adc == Ad
    display("Transformation matrix successfully verified")
end

