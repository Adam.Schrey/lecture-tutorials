disp('Abtastzeit')
Ts = 0.1

disp('Kontinuierliche Systeme')
G = tf(1,[1 1])

disp('Beispiel 1: ZOH')
Gh = c2d(G,Ts,'zoh')

disp('Beispiel 2: Impulse')     
Gz = c2d(G,Ts,'impulse')

disp('Bespiel 3: Verfahren G(s)/s')
Gi = tf([1],[1 0])
Gs = G*Gi
Gsz = c2d(Gs,Ts,'impulse')
Gh = tf([1 -1], [1 0],Ts)
Gf = Gh*Gsz/Ts;
minreal(Gf)



