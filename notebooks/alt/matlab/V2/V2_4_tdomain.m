Ts = 0.1;
p = -1;
npoint = 20;

y1 = longdiv([1 0],[1 -exp(Ts*p)],npoint);
t =(1:npoint)*Ts-Ts;

yc = impulse(1,[1 1],t);
plot(t,yc,'r',t,y1,'*')

hold on

yh = longdiv(1-exp(Ts*p),[1 -exp(Ts*p)],npoint);
plot(t,yh,'+')
