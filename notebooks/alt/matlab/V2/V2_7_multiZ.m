Ts = 0.1;
Gz = tf([1 0],[1 -exp(-0.1)],Ts);
%Impuls: longdiv(num,den,100)
y1=longdiv(cell2mat(Gz.num),cell2mat(Gz.den),100);
plot(y1);

hold on;

Uz = tf([1 0],[1 -1],Ts);
Yz = Gz*Uz;
y2=longdiv(cell2mat(Yz.num),cell2mat(Yz.den),100);
plot(y2);

Gz2 = tf([1-exp(-0.1) 0],[1 -1-exp(-0.1) exp(-0.1)],Ts);
y3=longdiv(cell2mat(Gz2.num),cell2mat(Gz2.den),100);
plot(y3);