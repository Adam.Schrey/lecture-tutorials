p1 = -5
p2 = -8;

Num = [p1*p2]
Den = [1 -(p1+p2) p1*p2]

Gs = tf(Num,Den)

bode(Gs)
pause

% PID 
omo = 10
Fi = 46*pi/180

[Go Fo] = bode(Gs,omo)
Fo = Fo*pi/180;

Kd = 0;
Kp = cos(-pi+Fi-Fo)/Go
Ki = -sin(-pi+Fi-Fo)*omo/Go


Gr = tf([Kd Kp Ki],[1 0]);

Go = Gs*Gr

bode(Go)
pause
nyquist(Go)
pause
margin(Go)

pause

Ts = 0.02
Gsz = c2d(Gs,Ts,'zoh');

Kr = Kp
Ti = Kr/Ki
Zn = 1/(1+Ts/Ti)
Numrz = [Kr/Zn -Kr]
Denrz = [1 -1]
Grz= tf(Numrz,Denrz,Ts)

Goz = Gsz*Grz
margin(Goz)

[Gzo Fzo] = bode(Goz,omo)
