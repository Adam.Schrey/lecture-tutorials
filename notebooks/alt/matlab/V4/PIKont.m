p1 = -5
p2 = -8;

Num = [p1*p2]
Den = [1 -(p1+p2) p1*p2]

Gs = tf(Num,Den)

bode(Gs)
pause

% PID 
omo = 10
Fi = 40*pi/180

[Go Fo] = bode(Gs,omo)
Fo = Fo*pi/180;

Kd = 0;
Kp = cos(-pi+Fi-Fo)/Go
Ki = -sin(-pi+Fi-Fo)*omo/Go


Gr = tf([Kd Kp Ki],[1 0]);

Go = Gs*Gr

bode(Go)
pause
nyquist(Go)
pause
margin(Go)