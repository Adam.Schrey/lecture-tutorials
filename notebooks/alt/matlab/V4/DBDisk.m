p1 = -5
p2 = -8;

Num = [p1*p2]
Den = [1 -(p1+p2) p1*p2]

Gs = tf(Num,Den)

bode(Gs)
pause


Ts = 0.02
Gsz = c2d(Gs,Ts,'zoh')

Kw = tf(1,[1 0 0],Ts);

Grz = Kw/(Gsz*(1-Kw))

Gl = Gsz*Grz;

Numz = cell2mat(Grz.num)
Denz = cell2mat(Grz.den)
