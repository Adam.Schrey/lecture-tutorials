% Frequenz Signal 1
om1 = 1

% Frequenz Signal 2
om2 = 1+2*pi

% Abtast Frequenz
om_T = 2*pi

% Abtast Zeit
T = 2*pi/om_T

maxk = 200
k=1:maxk
t=k.*T

%Signal 1
y1 = sin(om1*k*T)

%Signal 2
y2 = sin(om2*k*T)

plot(t,y1,t,y2)

