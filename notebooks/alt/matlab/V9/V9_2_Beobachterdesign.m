% System:
A = [-2 1;
      0 -1]
  
B = [0; 1]

C = [1 0]

D = 0;

% Controllability/Steuerbarkeit:
Sc = [B A*B]
rank(Sc)

% Observability/Beobachtbarkeit:
So = [C; C*A]
rank(So)

%Regler:
pc = [-5+1j*5 -5-1j*5]
K = place(A,B,pc)

%Beobachterpol:
csi = 0.8

%Grundfrequenz:
omega0 =10;
%omega0 = 1; 
poles = [-csi*omega0+1j*omega0*sqrt(1-csi*csi) -csi*omega0-1j*omega0*sqrt(1-csi*csi)]
G = place(A',C',poles)

%Beobachter Eigenwerte:
Ao = A-G'*C
eigs(Ao)
