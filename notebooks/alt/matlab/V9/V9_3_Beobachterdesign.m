% System
A = [-2 1;
      0 -1]
  
B = [0; 1]

C = [1 0]

D = 0;

% Steuerbarkeit/Controllability:
Sc = [B A*B]
rank(Sc)


%Zeitdiskrete System!
Ts = 0.01

pc = [-5+1j*5 -5-1j*5]
pcd = exp(pc.*Ts)
Ad = expm(A*Ts);
Bd = (Ad-eye(2))*inv(A)*B

%Reglerentwurf:
K = place(Ad,Bd,pcd) 

% Beobachtbarkeit/Observability:
So = [C; C*Ad]
rank(So)

%Beobachter Eignewerte:
poles = [0 0]; %Deadbeat Beobachter!
Gd = acker(Ad',C',poles) 

Aod = Ad-Gd'*C
eigs(Aod)
