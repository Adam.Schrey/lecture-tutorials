disp('Definition des System');
A = [-1 0 0;
    1 -2 0;
    0  5 0]

B = [1; 0; 0]


disp('Steuerbarkeit Matriz')
S_c = [B A*B A*A*B]

S_c_inv = inv(S_c)
s_c_n = S_c_inv(3,:)

disp('Pole Vorgabe')
disp('p1 = -2, p2,3= -1+/-J')
P = [4 6 4 1]
p1 = -2
p2 = -1+1j
p3 = -1-1j

v1 = s_c_n
v2 = s_c_n*A
v3 = s_c_n*A*A
v4 = s_c_n*A*A*A

F = (P(1)*v1+P(2)*v2+P(3)*v3+P(4)*v4)' %Wir erhalten den Rückführvektor
F2 = place(A,B,[p1 p2 p3]) %Rückführvektor nach Polvorgabe

