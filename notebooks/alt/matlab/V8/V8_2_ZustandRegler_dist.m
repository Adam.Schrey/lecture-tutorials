disp('Definition des System');
A = [1 1;
    -2   -1]

B = [0; 1]

C = [1 0]

D = 0

disp('Steuerbarkeit des System');
W = [B  A*B]
rank(W)

disp('Eigenwerte');
eigs(A)

disp('Regler');
pd = [-3+1j*3  -3-1j*3]
F = place(A,B,pd)

V = inv(C*inv(B*F-A)*B)


Ae = [A [0; 0]; -C 0]

Be = [B; 0]

Ce = [C 0]


pde = [-3+1j*3  -3-1j*3 -6]

G = place(Ae,Be,pde)


