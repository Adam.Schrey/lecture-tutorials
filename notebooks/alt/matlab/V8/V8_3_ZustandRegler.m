disp('Definition des System:');
A = [-1, 0, 0; 
    1, -2, 0; 
    0, 5, 0];

B = [1; 0; 0];

disp('Steuerbarkeit des System:');
S_c = [B,  A*B, A^2*B]
rank(S_c)

disp('Current eigenvalues:');
eigs(A)

S_c_inv = inv(S_c)
S_c_n = S_c_inv(end, :)'
 
disp('Desired poles');
p = [-2; (-1+1j); (-1-1j)]

disp('Rückführvektor (manuell):')
PA = (A-p(1)*eye(3)) * (A-p(2)*eye(3)) * (A-p(3)*eye(3))
F = S_c_n' * PA  %Rückführvektor

disp('Rückführvektor (mit place):')
F2 = place(A, B, poles) %Nach Polvorgabe ist F2 = F

disp('New eigenvalues:');
eigs(A-B*F)
