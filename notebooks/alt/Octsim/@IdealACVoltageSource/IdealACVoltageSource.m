classdef IdealACVoltageSource < RCComponent
    properties
        Vs
	om
	ph
        possource
    end
    methods
        function b = IdealACVoltageSource(p,n,V,omega,phase)
            b = b@RCComponent(p,n);
            b.Vs = V;         
            b.om = omega;
	   b.ph = phase;
        end
        
        function ApplyMatrixStamp(b,P,dt)
            ntot = P.GetSize();
            b.possource = ntot+1;
            P.G =  [P.G,zeros(ntot,1)];
            P.G =  [P.G; zeros(1,ntot+1)];
            P.b =   [P.b; 0];  
            if (b.Pos > 0)
                P.G(b.possource,b.Pos) = 1;
                P.G(b.Pos, b.possource) = 1;
            end
            if (b.Neg > 0)    
                P.G(b.possource,b.Neg) = -1;
                P.G(b.Neg,b.possource) = -1;
            end
        end
        
        function [Gout,Bout] = ApplyMatrixStampdp(b,P,dt,om)
            ntot = P.GetSize();
            b.possource = ntot+1;
            P.G =  [P.G,zeros(ntot,1)];
            P.G =  [P.G; zeros(1,ntot+1)];
            P.b =  [P.b; 0];  
            if (b.Pos > 0)
                P.G(b.possource,b.Pos) = 1;
                P.G(b.Pos, b.possource) = 1;
            end
            if (b.Neg > 0)    
                P.G(b.possource,b.Neg) = -1;
                P.G(b.Neg,b.possource) = -1;
            end
        end
        
        function Init(b,P,dt)
            b.ApplyMatrixStamp(P,dt);         
        end
        
        function Initdp(b,P,dt,om)
            b.ApplyMatrixStampdp(P,dt,om);         
        end
        
        function InitdpAdv(b,P,dt,om)
            b.ApplyMatrixStampdp(P,dt,om);         
        end
        
        function Step(b,P,dt,t)
            P.b(b.possource) = b.Vs*sin(b.om*t+b.ph);
        end
        
        function Stepdp(b,P,dt,t,om)
            P.b(b.possource) = b.Vs*exp(1j*b.ph)/sqrt(2);
        end
        
        function StepdpAdv(b,P,dt,t,om)
            P.b(b.possource) = b.Vs*exp(1j*b.ph)/sqrt(2);
        end
        
        function PostStep(b,vsol,dt)
        end
        
        function PostStepdp(b,vsol,dt,om)
        end
        
        function PostStepdpAdv(b,vsol,dt,om)
        end
    end
end
            
            
            
        
