classdef Diode < RCComponent
    properties
        Gr
        Ron 
        Roff
        Vnew
    end
    methods
        function r = Diode(p,n,Ron,Roff)
            r = r@RCComponent(p,n);
            r.Ron = Ron;
            r.Roff = Roff;
            r.linear = 0;
            r.Gr = 1/Ron;
            r.Vnew = 1;
        end   

        function r = Diode(p,n)
            r = r@RCComponent(p,n);
            r.Ron = 1e-4;
            r.Roff = 1e4;
            r.linear = 0;
            r.Gr = 1/r.Ron;
            r.Vnew = 1;
        end   



        function [Gout,Bout] = ApplyMatrixStamp(r,P,dt)
             
        end
        
        function Init(r,P,dt)
            r.ApplyMatrixStamp(P,dt); 
        end
        
        
        function Step(r,P,dt,t)
            
        end
        
        function MicroStep(r,P,dt,t,vt)
            if (r.Pos > 0)
                if (r.Neg > 0)
                    r.Vnew = vt(r.Pos)-vt(r.Neg);
                else
                    r.Vnew = vt(r.Pos);
                end
            else
                r.Vnew = -vt(r.Neg);
            end
            
            if r.Vnew > 0
                  r.Gr = 1/r.Ron;
            else
                  r.Gr = 1/r.Roff;
            end
                  
            if(r.Pos > 0)
                    P.G(r.Pos,r.Pos)=P.G(r.Pos,r.Pos)+r.Gr;
            end
            if(r.Neg > 0)
                    P.G(r.Neg,r.Neg)=P.G(r.Neg,r.Neg)+r.Gr;
            end
            if (r.Pos>0) && (r.Neg>0)
                P.G(r.Pos,r.Neg)=P.G(r.Pos,r.Neg)-r.Gr;
                P.G(r.Neg,r.Pos)=P.G(r.Neg,r.Pos)-r.Gr;
            end 
            
           
        end
        
        
        function PostStep(b,vsol,dt)
        end

    end
end
            
            
            
      
            
            
        
