classdef QuadResistance < RCComponent
    properties
        R  
        Gr
        Vnew
        Ar
    end
    methods
        function r = QuadResistance(p,n,par)
            r = r@RCComponent(p,n);
            r.R = par;
            r.linear = 0;
        end   
        
        function [Gout,Bout] = ApplyMatrixStamp(r,P,dt)
             
        end
        
        function Init(r,P,dt)
            r.ApplyMatrixStamp(P,dt); 
            r.Vnew = 0;
        end
        
        
        function Step(r,P,dt,t)
            
        end
        
        function MicroStep(r,P,dt,t,vt)
            if (r.Pos > 0)
                if (r.Neg > 0)
                    r.Vnew = vt(r.Pos)-vt(r.Neg);
                else
                    r.Vnew = vt(r.Pos);
                end
            else
                r.Vnew = -vt(r.Neg);
            end
            
            r.Gr = 2*r.Vnew/r.R;
            r.Ar = r.Vnew^2/r.R-r.Gr*r.Vnew;
            if(r.Pos > 0)
                    P.G(r.Pos,r.Pos)=P.G(r.Pos,r.Pos)+r.Gr;
            end
            if(r.Neg > 0)
                    P.G(r.Neg,r.Neg)=P.G(r.Neg,r.Neg)+r.Gr;
            end
            if (r.Pos>0) & (r.Neg>0)
                P.G(r.Pos,r.Neg)=P.G(r.Pos,r.Neg)-r.Gr;
                P.G(r.Neg,r.Pos)=P.G(r.Neg,r.Pos)-r.Gr;
            end 
            
            if (r.Pos > 0)
               P.b(r.Pos)= P.b(r.Pos)-r.Ar;
            end
             if (r.Neg > 0)
               P.b(r.Neg)= P.b(r.Neg)+r.Ar;          
            end
            
            
        end
        
        
        function PostStep(b,vsol,dt)
        end

    end
end
            
            
            
        
