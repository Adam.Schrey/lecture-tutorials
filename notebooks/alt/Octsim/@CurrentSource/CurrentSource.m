classdef CurrentSource < RCComponent
    properties
        Cur  
    end
    methods
        function b = CurrentSource(p,n,val)
            b = b@RCComponent(p,n);
            b.Cur = val;            
        end
        
        function ApplyMatrixStamp(b,P,dt)
          
        end
        
        function Init(b,P,dt)
            b.ApplyMatrixStamp(P,dt);         
        end
        
        function Step(b,P,dt,t)
         if (b.Pos > 0)
               P.b(b.Pos)= P.b(b.Pos)+b.Cur;
         end
        if (b.Neg > 0)
               P.b(b.Neg)= P.b(b.Neg)-b.Cur;          
        end
        
        end
        
        function PostStep(b,vsol,dt)
        end

    end
end
            
            
            
        
