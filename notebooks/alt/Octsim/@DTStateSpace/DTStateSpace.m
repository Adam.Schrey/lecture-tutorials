classdef DTStateSpace < DTBlock
    properties
        A
        B
        C
        D
        xo 
    end
    methods
        function c = DTStateSpace(in,out,A,B,C,D,T)
            c = c@DTBlock();
            c.A = A;
            c.B = B;
            c.C = C;
            c.D = D;
            c.Ts = T;
            c.ninput = size(B,2);
            c.noutput = size(C,1);
            c.inpos = in;
            c.outpos = out;
            c.nstate = size(c.A,1);
            c.xo = zeros(1,c.nstate);
            c.x = c.xo;
        end 
        
        function c = DTStateSpace(in,out,A,B,C,D,T,xo)
            c = c@DTBlock();
            c.A = A;
            c.B = B;
            c.C = C;
            c.D = D;
            c.Ts = T;
            c.ninput = size(B,2);
            c.noutput = size(C,1);
            c.inpos = in;
            c.outpos = out;
            c.nstate = size(c.A,1);
            c.x = xo;
            c.xo = xo;
        end 
        
        function flag = Reset(c)
            c.x = c.xo;
            flag = 1;
        end
        
        
        function updatediscrete(c)
            c.y = c.C*c.x+c.D*c.u';
            c.x=c.A*c.x+c.B*c.u';      
            flag = 1;
        end
        
    end
end
            
            
            
        
