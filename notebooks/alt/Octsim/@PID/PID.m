classdef PID < Block
    properties
        A
        B
        C
        D
        Kd
        olderr
        ini
    end
    methods
        function c = PID(in,out,Kp,Ki,Kd,ini)
            c = c@Block();
            c.A = 0;
            c.B = 1;
            c.C = Ki;
            c.D = Kp;
            c.Kd = Kd;
            c.ninput = 1;
            c.noutput = 1;
            c.inpos(1) = in;
            c.outpos(1) = out;
            c.nstate = 1;
            c.x = ini;
            c.ini = ini;
            c.olderr = 0;
         end 
        
        function flag = Step(c,t,dt)
            c.y = c.C*c.x+c.D*c.u;
            c.y = c.y+c.Kd*(c.u-c.olderr)/dt;
            c.olderr = c.u;
            c.updatestate(t,dt);      
            flag = 1;
        end
        
        function [dxdt] = cdxdt(c,x,t)
            dxdt = c.A*x+c.B*c.u;
        end
        
         function flag = Reset(c)
            c.x = c.ini;
            c.olderr = 0;
            flag = 1;
        end
        
        function flag = ChangeParameters(c,value)
           Kp = value(1);
           Ki = value(2);
           Kd = value(3);
           c.ini = value(4);
           
           c.A = 0;
           c.B = 1;
           c.C = Ki;
           c.D = Kp;
           c.Kd = Kd;
           
           flag = 1;
        end
        
        
    end
end
            
            