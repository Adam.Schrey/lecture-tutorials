classdef HybridSystem < handle
    
    properties 
        nw
        sc
        ti
        tf
        dt
        t
        maxn
        toll
        vk
    end
    
    methods
        function hy = HybridSystem(nnode,numflows,ti,tf,dt,maxn,toll)
           hy.nw = Circuit(nnode);
           hy.sc = Schema(ti,tf,dt,numflows);
           hy.dt = dt;
           hy.ti = ti;
           hy.tf = tf;
           hy.maxn = maxn;
           hy.toll = toll;
        end
        
        
        function AddComponent2Network(hy,h)
            hy.nw.AddComponent(h);       
        end
        
        function AddComponent2Schema(hy,h)
            hy.sc.AddComponent(h);       
        end
        
        
        function Init(hy)
            hy.nw.Initnl(hy.dt);
            hy.sc.Init();
            hy.vk = zeros(hy.nw.n,1);
            hy.t = hy.ti;
        end
        
        
        
        
        function flagend = Step(hy)
            t = hy.t;
            k=1;
            vkold = hy.vk;
            hy.nw.Plin.Resetb();
            flagnl = 0;
            for i=1:hy.nw.nelements
                if (hy.nw.NetList{i}.hyin > 0)
                    hy.nw.NetList{i}.ReadFlowIn(hy.sc.flows);
                end 
                if (hy.nw.NetList{i}.linear > 0)
                    hy.nw.NetList{i}.Step(hy.nw.Plin,hy.dt,t);
                else
                    flagnl = 1;
                end 
            end
            iter =1;
            err = 1e5;
            if flagnl == 1    
                while iter<=hy.maxn && (err>=hy.toll) 
                    hy.nw.Pnl = LinProb(hy.nw.Plin.GetSize());
                    for i=1:hy.nw.nelements
                    if (hy.nw.NetList{i}.linear == 0)
                    	hy.nw.NetList{i}.MicroStep(hy.nw.Pnl,hy.dt,hy.t,vkold);
                    end 
                    end
                    
                    hy.nw.Pn = hy.nw.Plin+hy.nw.Pnl;
                  
                    
                    hy.vk = hy.nw.Pn.Solve();
          
                    
                    delta = hy.vk-vkold;
                    err = delta'*delta;
                    vkold = hy.vk;
                    iter = iter+1;
                end
            else
                    hy.vk = hy.nw.Plin.Solve(); 
            end
                
             for i=1:hy.nw.nelements
                    hy.nw.NetList{i}.PostStep(hy.vk,hy.dt);
                    if (hy.nw.NetList{i}.hyout > 0)
                    hy.nw.NetList{i}.WriteFlowOut(hy.sc.flows);
                end 

             end   
             hy.sc.Step();
             hy.t = hy.t+hy.dt;
            if (hy.t < hy.tf)
                    flagend = 1;
            else
                    flagend = 0;
            end
            
        end
             
        function val = GetFlow(hy,n)
            val = hy.sc.flows(n);
        end  
        
        function val = GetNode(hy,n)
            val = hy.vk(n);
        end  
           
        function val = GetTime(hy)
            val = hy.t;
        end
    end

    
end
            
            
            