classdef VCVS < RCComponent
    properties
        cpos  
        cneg
        gain
    end
    methods
        function v = VCVS(p,n,cp,cn,g)
            v = v@RCComponent(p,n);
            v.gain = g;
            v.cpos = cp;
            v.cneg = cn;
           
        end   
        
        function ApplyMatrixStamp(v,P,dt)
            ntot = P.GetSize();
            b.possource = ntot+1;
            P.G =  [P.G,zeros(ntot,1)];
            P.G =  [P.G; zeros(1,ntot+1)];
            P.b =   [P.b; 0];  
            if (b.Pos > 0)
                P.G(b.Pos, b.possource) = 1;
                P.G(b.possource,b.Pos) = 1;            
            end
            if (b.Neg > 0)    
                P.G(b.possource,b.Neg) = -1;
                P.G(b.Neg,b.possource) = -1;
            end
            if(v.cpos > 0)
                    P.G(v.possource,v.cpos)=P.G(v.possource,v.cpos)-v.gain;
            end
            if(v.cneg > 0)
                    P.G(v.possource,v.cneg)=P.G(v.possource,v.cneg)+v.gain;
            end
            
        end
        
        function Init(v,P,dt)
            v.ApplyMatrixStamp(P,dt);         
        end
        
        function Step(v,P,dt,t)
            
        end
        
        function PostStep(v,vsol,dt)
        end

    end
end
            
            
            
        
