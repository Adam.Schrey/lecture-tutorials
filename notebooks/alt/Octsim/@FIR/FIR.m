classdef FIR < DTBlock
    properties
        a
        n
        buf
    end
    methods
        function c = FIR(in,out,a,T)
            c = c@DTBlock();
            c.a = a;
            c.Ts = T;
            c.ninput = 1;
            c.noutput = 1;
            c.inpos = in;
            c.outpos = out;
            c.n = size(c.a,2);
            c.buf = zeros(c.n,1);
        end 
        
        function updatediscrete(c)
            for i=c.n:-1:2
                c.buf(i) = c.buf(i-1);
            end
            c.buf(1) = c.u;    
            c.y = c.a*c.buf;      
            flag = 1;
        end
        
        function flag = Reset(b);
            b.buf = zeros(b.n,1);
            flag = 1;
        end
        
    end
end
            
            
            
        
