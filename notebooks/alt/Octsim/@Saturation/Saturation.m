classdef Saturation < Block
    properties
        min
        max
    end
    methods
        function c = Saturation(in,out,min,max)
            c = c@Block();
            c.ninput = 1
            c.noutput = 1;
            c.inpos(1) = in;
            c.outpos(1) = out;
           c.min = min;
           c.max = max;
         end 
        
        function flag = Step(c,t,dt)
            c.y = c.u;
            if c.y > c.max
                c.y = c.max;
            end
            if c.y < c.min
                c.y = c.min;
            end
            flag = 1;
        end
        
        flag = ChangeParameters(c,value)
            c.min = value(1);
            c.max = value(2);
            flag = 1;
        end
        
        
    end
end
            
            
            
        
