classdef Not < Block
    properties
    end
    methods
        function c = Not(in,out)
            c = c@Block();
            c.ninput = 1;
            c.inpos(1) = in;
            c.noutput = 1;
            c.outpos(1) = out;
        end 
        
        function flag = Step(c,t,dt)
            if c.u > 0
                c.y = 0;
            else
                c.y = 1;
            end
            flag = 1;
        end
        
    end
end
            
            
            
        
