classdef Capacitor < RCComponent
    properties
        C  
        Gc
        Bc
        Ic
        Vc
    end
    methods
        function c = Capacitor(p,n,par)
            c = c@RCComponent(p,n);
            c.C = par;
        end
        
        function ApplyMatrixStamp(c,P,dt)
                        c.Gc = 2*c.C/dt;
            if(c.Pos > 0)
                    P.G(c.Pos,c.Pos)=P.G(c.Pos,c.Pos)+c.Gc;
            end
            if(c.Neg > 0)
                    P.G(c.Neg,c.Neg)=P.G(c.Neg,c.Neg)+c.Gc;
            end
            if (c.Pos>0) & (c.Neg>0)
                P.G(c.Pos,c.Neg)=P.G(c.Pos,c.Neg)-c.Gc;
                P.G(c.Neg,c.Pos)=P.G(c.Neg,c.Pos)-c.Gc;
            end       
        end
        
        function ApplyMatrixStampdpAdv(c,P,dt,om)
            a0 = 1;
            b0 = dt/2+1j*(1/om-dt/2*cot(om*dt/2));
            b1 = dt/2 - 1j*(1/om-dt/2*cot(om*dt/2));
            c.Gc = c.C*(1+1j*om*b1)/b1;
            if(c.Pos > 0)
                    P.G(c.Pos,c.Pos)=P.G(c.Pos,c.Pos)+c.Gc;
            end
            if(c.Neg > 0)
                    P.G(c.Neg,c.Neg)=P.G(c.Neg,c.Neg)+c.Gc;
            end
            if (c.Pos>0) & (c.Neg>0)
                P.G(c.Pos,c.Neg)=P.G(c.Pos,c.Neg)-c.Gc;
                P.G(c.Neg,c.Pos)=P.G(c.Neg,c.Pos)-c.Gc;
            end       
        end
        
        function ApplyMatrixStampdp(c,P,dt,om)
            
            c.Gc = 2*c.C/dt+1j*om*c.C;
            if(c.Pos > 0)
                    P.G(c.Pos,c.Pos)=P.G(c.Pos,c.Pos)+c.Gc;
            end
            if(c.Neg > 0)
                    P.G(c.Neg,c.Neg)=P.G(c.Neg,c.Neg)+c.Gc;
            end
            if (c.Pos>0) & (c.Neg>0)
                P.G(c.Pos,c.Neg)=P.G(c.Pos,c.Neg)-c.Gc;
                P.G(c.Neg,c.Pos)=P.G(c.Neg,c.Pos)-c.Gc;
            end       
        end
        
        
        function Init(c,P,dt)
            c.ApplyMatrixStamp(P,dt);        
            c.Bc = 0;
            c.Vc = 0;
            c.Ic = 0;
        end
        
        function Initdp(c,P,dt,om)
            c.ApplyMatrixStampdp(P,dt,om);        
            c.Bc = 0;
            c.Vc = 0;
            c.Ic = 0;
        end
        
        function InitdpAdv(c,P,dt,om)
            c.ApplyMatrixStampdpAdv(P,dt,om);        
            c.Bc = 0;
            c.Vc = 0;
            c.Ic = 0;
        end
        
        function Step(c,P,dt,t)
            c.Bc = c.Gc*c.Vc+c.Ic;
            if (c.Pos > 0)
               P.b(c.Pos)= P.b(c.Pos)+c.Bc;
            end
             if (c.Neg > 0)
               P.b(c.Neg)= P.b(c.Neg)-c.Bc;          
            end
        end
        
        function Stepdp(c,P,dt,t,om)
            c.Bc = (2/dt*c.C*-1j*om*c.C)*c.Vc+c.Ic;
            if (c.Pos > 0)
               P.b(c.Pos)= P.b(c.Pos)+c.Bc;
            end
             if (c.Neg > 0)
               P.b(c.Neg)= P.b(c.Neg)-c.Bc;          
            end
        end
        
        function StepdpAdv(c,P,dt,t,om)
            a0 = 1;
            b0 = dt/2+1j*(1/om-dt/2*cot(om*dt/2));
            b1 = dt/2 - 1j*(1/om-dt/2*cot(om*dt/2));
            c.Bc = (-a0+1j*om*b0)*c.C*c.Vc/b1-b0*c.Ic/b1;
            if (c.Pos > 0)
               P.b(c.Pos)= P.b(c.Pos)-c.Bc;
            end
             if (c.Neg > 0)
               P.b(c.Neg)= P.b(c.Neg)+c.Bc;          
            end
        end
        
        function PostStep(c,vsol,dt)
            if (c.Pos > 0)
                if (c.Neg > 0)
                    c.Vc = vsol(c.Pos)-vsol(c.Neg);
                else
                    c.Vc = vsol(c.Pos);
                end
            else
                c.Vc = -vsol(c.Neg);
            end
                    
            c.Ic = c.Gc*c.Vc-c.Bc;
        end
        
        function PostStepdp(c,vsol,dt,om)
            if (c.Pos > 0)
                if (c.Neg > 0)
                    c.Vc = vsol(c.Pos)-vsol(c.Neg);
                else
                    c.Vc = vsol(c.Pos);
                end
            else
                c.Vc = -vsol(c.Neg);
            end
                    
            c.Ic = c.Gc*c.Vc-c.Bc;
        end

        function PostStepdpAdv(c,vsol,dt,om)
            a0 = 1;
            b0 = dt/2+1j*(1/om-dt/2*cot(om*dt/2));
            b1 = dt/2 - 1j*(1/om-dt/2*cot(om*dt/2));
            if (c.Pos > 0)
                if (c.Neg > 0)
                    c.Vc = vsol(c.Pos)-vsol(c.Neg);
                else
                    c.Vc = vsol(c.Pos);
                end
            else
                c.Vc = -vsol(c.Neg);
            end
                    
            c.Ic = c.Gc*c.Vc+c.Bc;
        end
    end
end
            
            
            
        
